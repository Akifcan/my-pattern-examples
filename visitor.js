class Employee {
  constructor(name, salary) {
    this.name = name
    this.salary = salary
  }

  getSalary() {
    return this.salary
  }

  setSalary(newSalary) {
    this.salary = newSalary
  }

  accept(visitorFunction) {
    visitorFunction(this)
  }
}

const devSage = new Employee('devsage', 10000)

console.log(devSage.getSalary())

function extraSalary(emp) {
  emp.setSalary(emp.getSalary() * 2)
}

devSage.accept(extraSalary)
console.log(devSage.getSalary())
