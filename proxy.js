class CryptoCurrency{
	getValue(coin){
  	console.log('calling extarnal api!')
    switch(coin){
    	case "Bitcoin":
      	return 500
      case "Litecoin":
      	return 400
      case "Mana":
      	return 155
    }
  }
}

const api = new CryptoCurrency()

class CryptoCurrencyProxy{

constructor(){
 	  this.api = new CryptoCurrency()
  	this.cache = {}  
  }
  
  getValue(coin){
  	if(this.cache[coin] == null){
    	console.log('saved to cache')
    	this.cache[coin] = this.api.getValue(coin)
    	return this.cache[coin]
    }
    console.log('fetched from cache!')
    return this.cache[coin]
  } 
}

const proxy = new CryptoCurrencyProxy()

console.log(proxy.getValue('Bitcoin'))
console.log(proxy.getValue('Mana'))
console.log(proxy.getValue('Litecoin'))
console.log(proxy.getValue('Litecoin'))
console.log(proxy.getValue('Litecoin'))
console.log(proxy.getValue('Bitcoin'))