function retryOperation(fn, max=5) {
  let currentTry = 0
  while (true && currentTry < max) {
		try {
    	return fn()
      break;
    }catch(error){
    	console.log('an error occured!', currentTry)
      currentTry++
    }
  }
  console.log('stopped!')
}

function externalServiceCall(){
	const num = Math.floor(Math.random() * 99)
	if(num < 5){
  return num
  }
      throw 'Parameter is not a number!';

}

console.log(retryOperation(externalServiceCall))