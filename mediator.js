class Member {
  constructor(name) {
    this.name = name
    this.chatroom = null
  }
  send(message, toMember) {
    this.chatroom.send(message, this, toMember)
  }
  receive(message, fromMember) {
    console.log(fromMember.name, this.name, message)
  }
}



class ChatRoom {
  addMember(member) {
    member.chatroom = this
  }
  send(message, fromMember, toMember) {
    toMember.receive(message, fromMember)
  }
}


const chat = new ChatRoom()
const bob = new Member('Bob')
const john = new Member('John')
const tim = new Member('Tim')

chat.addMember(bob)
chat.addMember(john)
chat.addMember(tim)

bob.send('hey john', john)
